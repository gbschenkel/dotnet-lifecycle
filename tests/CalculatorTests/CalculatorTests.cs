public class CalculatorFixture : IDisposable
{
   public CalculatorFixture()
   {
   }

   public void Dispose()
   {
   }
}

public class CalculatorTests : CalculatorFixture
{
    [Fact]
    public void Add_PositiveNumbers_ReturnsExpectedResult()
    {
       // Arrange
       var calculator = new Calculator();
       int a = 3;
       int b = 5;
       int expectedResult = 8;

       // Act
       int actualResult = calculator.Add(a, b);

       // Assert
       Assert.Equal(expectedResult, actualResult);
    }

    [Theory]
    [InlineData(1, 2, 3)]
    [InlineData(-1, -2, -3)]
    public void Add_TowNumbers_ReturnsSum(int a, int b, int expectedResult)
    {
       // Arrange
       var calculator = new Calculator();

       // Act
       int actualResult = calculator.Add(a, b);

       // Assert
       Assert.Equal(expectedResult, actualResult);
    }
}